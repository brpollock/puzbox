ALL: TEST cmain

TEST: test_p3x3.py p3x3.py Makefile
	python -m unittest test_p3x3

cmain: cmain.c bricks.x Makefile
	gcc -O3 -o cmain cmain.c

bricks.x: bricks.py mkcdata.py p3x3.py Makefile
	python3 mkcdata.py 
