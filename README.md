# 3x3 puzzle problem

There's a wooden puzzle with seven pieces which
can be arranged to form a solid 3x3 cube.

The question is: how many solutions are there?

Each of the puzzle pieces can be thought of as
a set of either 3 or 4 small (1x1) cubes (or
voxels) glued together.

Only one of the pieces has 3 voxels:

    6 x 4 + 1 x 3 = 27 voxels

## The bricks

The bricks are defined in bricks.py as lists
of voxel positions.

To show a 3D block on a 2D grid, z-positions
0,1,2 are shown as digits 1,2,4. And when there's
multiple voxels at the same x,y their digits
are summed.

```
# BRICK
# 31- 
# 1-- 
# --- 
(0, 0, 0)
(0, 1, 0)
(0, 0, 1)
(1, 0, 0)
```


## The approach

1. For each piece (brick)
    - create all (24) orientations
    - remove any duplicates
2. For each orientation (of each piece):
    - calculate all possible translations
3. Brute-force try all combinations of
   rotations/translations.

Note: Rotations are created in two stages:

1. the tops() function rotates the cube,
   like a dice, so that you see each of the
   six faces on top once.
2. the rots() function takes a cube with
   _sone_ face on top and produces each
   of the four rotations around the z-axis
   (which keeps the same face on top).

## The animation

The solutions produced by the solvers below were used
to drive this [Processing animation](https://openprocessing.org/sketch/818240).

## Running the solver

```
python main.py
```

## Using the C solver

The C solver uses exactly the same python code to
produce lists of oriented/translated pieces, but the
brute force stage is faster.

> Note: both solvers are pretty fast now

To build the lists and the solver:

```
make
```

To run the C solver

```
./cmain
```

To capture all the solutions:

In cmain.c, ensure `show_progress' and show_2dmap are false, and `show_solution` is true.
```
const int show_progress = 0;
const int show_solution = 1;
const int show_2dmap    = 0;
```

Then, build and run:

```
make
./cmain >mysolutions
```

(see cmain.c for details of the solution format).

## Notes

To add quotes and commas to a solutions file:

```
awk '{print"\""$1"\","}' mysolutions
```
