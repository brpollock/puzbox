"""
Given bricks defined as lists of voxels:
- calculate all unique and valid
    - positions
    - orientations
- convert into lists of voxel/bit-maps:
    - A[],B[],...G[]
"""

from p3x3 import *

exclude_fixed_brick(None)

FIXED_BRICK0 = True
ROTATE_BRICK1 = True

assert (FIXED_BRICK0,ROTATE_BRICK1) != (True,False)

  # BRICK 0
  #  111 
  #  -1-
  #  ---
if FIXED_BRICK0:
    # we only need one version of brick0
    # (see theorem.md)
    A = brick([
        (0, 0, 0),
        (1, 0, 0),
        (1, 1, 0),
        (2, 0, 0),],
        rotateme=False,
        translateme=False)
    # since this brick is fixed there's
    # no point adding any bricks that clash
    # with it to the other lists
    exclude_fixed_brick(A[0])
else:
    A = brick([
        (0, 0, 0),
        (1, 0, 0),
        (1, 1, 0),
        (2, 0, 0),])
  
  # BRICK 1
  #  111
  #  1--
  #  ---
B = brick([
    (0, 0, 0),
    (0, 1, 0),
    (1, 0, 0),
    (2, 0, 0),],
    rotateme=ROTATE_BRICK1,
    # we can exclude all translations that occupy the
    # centre of the cube (see theorem.md)
    exclude_centre=True)
  
  # BRICK 2
  #  11-
  #  1--
  #  ---
C = brick([
    (0, 0, 0),
    (0, 1, 0),
    (1, 0, 0),])
  
  
  # BRICK 3
  #  11-
  #  -11
  #  ---
D = brick([
    (0, 0, 0),
    (1, 0, 0),
    (1, 1, 0),
    (2, 1, 0),])
  
  # BRICK 4
  #  11-
  #  3--
  #  ---
E = brick([
    (0, 0, 0),
    (1, 0, 0),
    (0, 1, 0),
    (0, 1, 1),])
  
  # BRICK 5
  #  11-
  #  -3-
  #  ---
F = brick([
    (0, 0, 0),
    (1, 0, 0),
    (1, 1, 0),
    (1, 1, 1),])
  
  # BRICK 6
  #  31-
  #  1--
  #  ---
G = brick([
    (0, 0, 0),
    (0, 1, 0),
    (0, 0, 1),
    (1, 0, 0),])

bricklists = [A,B,C,D,E,F,G]


""" ---------------------------------------------------
    another set
"""

exclude_fixed_brick(None)


  # BRICK 0
  #  11- 
  #  ---
  #  ---
A2 = brick([
    (0, 0, 0),
    (1, 0, 0),])
  
  # BRICK 1
  #  11-
  #  1--
  #  ---
B2 = brick([
    (0, 0, 0),
    (1, 0, 0),
    (0, 1, 0),])
  
  # BRICK 2
  #  11-
  #  11-
  #  ---
C2 = brick([
    (0, 0, 0),
    (1, 0, 0),
    (0, 1, 0),
    (1, 1, 0),])
  
  
  # BRICK 3
  #  111
  #  1-1
  #  ---
D2 = brick([
    (0, 0, 0),
    (1, 0, 0),
    (2, 0, 0),
    (0, 1, 0),
    (2, 1, 0),])
  
  # BRICK 4
  #  1--
  #  11-
  #  111
E2 = brick([
    (0, 0, 0),
    (0, 1, 0),
    (1, 1, 0),
    (0, 2, 0),
    (1, 2, 0),
    (2, 2, 0),])
  
  # BRICK 5
  #  1--
  #  111
  #  111
F2 = brick([
    (0, 0, 0),
    (0, 1, 0),
    (1, 1, 0),
    (2, 1, 0),
    (0, 2, 0),
    (1, 2, 0),
    (2, 2, 0),],
    rotateme=False)
  
  
bricklists2 = [A2,B2,C2,D2,E2,F2]


