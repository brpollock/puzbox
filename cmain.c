/* 3x3 puzzle solver
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "bricks.x"

#define MAX_NUM_PIECE   10

const int show_progress = 0;
const int show_solution = 1;
const int show_2dmap    = 0;


/* format a solution for output

  e.g. 511523022413553006413446066

where:
  x =  012012012012012012012012012
  y =  000111222000111222000111222
  z =  000000000111111111222222222

or:
  y\z   0   1   2

  0    511 413 413
  1    523 553 446
  2    022 006 066

*/


char *make_solution(int np, int p[])
{
    int i;
    static char buf[27+1];

    for(i = 0;i < 27;i++) {
        int k;
        int m = 1 << i;
        for(k = 0;k < np;k++) {
            if(p[k] & m)
                buf[i] = '0'+k;
        }
    }
    buf[27] = 0;
    return buf;
}

/* render each tile as 3x3 ascii 'bitmap'
*/

char *make_2dmap(int np, int p[])
{
    static char buf[3*(MAX_NUM_PIECE*5+1)+1];
    int x,y,z,k;
    int pitch = np*5+1;

    memset(buf,0,sizeof(buf));
     
    int m = 1;
    for(z = 0;z < 3;z++) {
        for(y = 0;y < 3;y++) {
            for(x = 0;x < 3;x++) {
                for(k = 0;k < np;k++) {
                    if(p[k] & m) buf[y*pitch + k*5+x] |= (1<<z);
                }
                m <<= 1;
            }
        }
    }

    for(y = 0;y < 3;y++) {
        int b;
        for(b = 0;b < np;b++) {
            for(x = 0;x < 5; x++) {
                int j = y*pitch + b*5 +x;
                if(x > 2) buf[j] = ' ';
                else if(buf[j]==0) buf[j] = '-';
                else buf[j] += '0';
            }
        }
        buf[y*pitch + np*5] = '\n';
    }
    buf[3*pitch] = 0;
    return &buf[0];
}

/* just a list of pieces
*/

char *make_progress(int np,int p[])
{
    static char buf[128];
    int i;

    buf[0] = 0;
    for(i = 0;i < np;i++) {
        sprintf(buf+strlen(buf)," %d",p[i]);
    }
    return buf;
}


int g_count = 0;

int solve(int **bls, int bits, int np, int p[])
{
    int count = 0;
    int i,b;

    if(!bls[0]) {
        ++g_count;
        if(show_progress) printf("%d:%s ",
            g_count, make_progress(np,p));
        if(show_solution) printf("%s",make_solution(np,p));
        if(show_2dmap) printf("\n%s",make_2dmap(np,p));
        if(show_progress || 
            show_solution ||
            show_2dmap) printf("\n");
        return 1;
    }

    for(i = 0;(b = bls[0][i]) > 0;++i) {
        if(!(b & bits)) {
            int pieces[np + 1];
            if(np)
                memcpy(pieces, p, np*sizeof(int));
            pieces[np] = b;
            count += solve(bls+1, bits|b, np+1, pieces);
        }
    }

    return count;
}


int main(void)
{
    fprintf(stderr,"total: %d\n",solve(bricklists,0,0,0));
    //fprintf(stderr,"total: %d\n",solve(bricklists2,0,0,0));
    return 0;
}

