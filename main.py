"""
 3x3 puzzle solver
"""

from __future__ import print_function

from bricks import bricklists,bricklists2


def solve(blists,bits=0,pieces=()):
    solutions = []
    if len(blists)==0:
        print(pieces)
        return [pieces]
    for b in blists[0]:
        if not b & bits:
            solutions += solve(blists[1:], bits|b, pieces+(b,))
    return solutions
 

print(len(solve(bricklists)))
#print(len(solve(bricklists2)))
