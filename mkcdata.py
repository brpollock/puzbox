"""
 3x3 puzzle solver
 - convert brick data to C

"""

from p3x3 import *

allow_duplicates(False)
from bricks import *


def write_data(f,L,X):
    f.write("int "+L+"[] = {")
    n = 0
    for x in X:
        f.write(str(x)+",")
        n = (n + 1 ) % 8
        if n == 0:
            f.write("\n")
    f.write("-1};\n\n")
    print(L,len(X))


with open("bricks.x","wt") as f:
    write_data(f,"A",A)
    write_data(f,"B",B)
    write_data(f,"C",C)
    write_data(f,"D",D)
    write_data(f,"E",E)
    write_data(f,"F",F)
    write_data(f,"G",G)
    f.write("int * bricklists[] = {"
            "A, B, C, D, E, F, G, 0};\n")
    write_data(f,"A2",A2)
    write_data(f,"B2",B2)
    write_data(f,"C2",C2)
    write_data(f,"D2",D2)
    write_data(f,"E2",E2)
    write_data(f,"F2",F2)
    f.write("int * bricklists2[] = {"
            "A2, B2, C2, D2, E2, F2, 0};\n")

