"""
create/render one brick in all orientations
"""

from __future__ import print_function

from p3x3 import *

  # BRICK 5
  #  11-
  #  -3-
  #  ---
b = [
    (0, 0, 0),
    (1, 0, 0),
    (1, 1, 0),
    (1, 1, 1),]

print("tops")
show_bricks(tops(b))

print("rots")
show_bricks(rots(b))

print("orients")
show_bricks(orients(b))
print(len(orients(b)))

print("unique_orients")
show_bricks(unique(orients(b)))
print(len(unique(orients(b))))

print("locs")
show_bricks(locs(b))
print(len(locs(b)))

show_brick(b)
print("bitmap",bin(bitmap(b)))

# check rotation with larger tuples
b = [(0,0,0,'x')]
print("rotx()",rotx(b))
print("roty()",roty(b))
print("rotz()",rotz(b))
