from __future__ import print_function


duplicates_allowed = False
fixed_brick = None

def allow_duplicates(val):
    global duplicates_allowed
    duplicates_allowed = val


def exclude_fixed_brick(fixed):
    global fixed_brick
    fixed_brick = fixed

def show_brick(voxels,buffers=None,space=" "):
    """render voxels as stacks of bits"""
    for y in range(3):
        s = ""
        for x in range(3):
            b = 0
            for z in range(3):
                if (x,y,z) in voxels:
                    b += 1 << z
            s += str(b) if b else "-"
        if buffers:
            buffers[y] += s + space
        else:
            print(s)


def _show_bricks(bricks,space):
    buffers = ["","",""]
    for brick in bricks:
        show_brick(brick,buffers,space)
    for b in buffers:
        print(b)
    print()


def show_bricks(voxels,space=" ",columns=12):
    if not columns:
        _show_bricks(voxels, space)
    else:
        v = voxels
        while v:
            _show_bricks(v[:columns],space)
            v = v[columns:]


def rotaxis(voxels,axis):
    """rotate voxels around _axis_
       NOTE: any fields beyond x,y,z
             are preserved.
       e.g.  (x,y,z,A) -> (x',y',z',A)
    """
    r = []
    for p in voxels:
        q = list(p)
        q[(axis + 1)%3] = 2 - p[(axis + 2) % 3]
        q[(axis + 2)%3] = p[(axis + 1) % 3]
        r.append(tuple(q))
    return r


def rotz(voxels):
    """rotate voxels around z axis"""
    return rotaxis(voxels,2)


def roty(voxels):
    """rotate voxels around y axis"""
    return rotaxis(voxels,1)


def rotx(voxels):
    """rotate voxels around x axis"""
    return rotaxis(voxels,0)


def bitmap(voxels):
    """encode voxels as a 27-bit number"""
    b = 0
    for x,y,z in voxels:
        b += 1 << (x + y*3 + z*9)
    return b


def brick(voxels,rotateme=True,translateme=True,exclude_centre=False):
    """create all orientations and positions
       of a brick as a list of bitmaps
    """
    bs = ["# ","# ","# "]
    print("# BRICK")
    show_brick(voxels,bs)
    for b in bs:
        print(b)
    for v in voxels:
        print(v)
    print()
    os = orients(voxels) if rotateme else [voxels]
    us = unique(os)
    print("all orientations (%d):" % len(os))
    if not rotateme: print("(rotation disabled)")
    show_bricks(os,"  ")
    print("..at origin:")
    show_bricks(origins(os),"  ")
    print("..without duplicates:")
    show_bricks(zero_dup(origins(os)),"  ")
    print("unique orientations (%d):" % len(us))
    show_bricks(us,"  ")
    print("translations of unique orientations:",
        "(centre excluded)" if exclude_centre else "")
    if not translateme: print("(translation disabled)")
    b = []
    for o in us:
        fixed_brick_clash = []
        ls = locs(o,translateme,exclude_centre)
        show_bricks(ls,"  ")
        for l in ls:
            bm = bitmap(l)
            if fixed_brick and (bm & fixed_brick):
                fixed_brick_clash.append(l)
                continue
            b.append(bitmap(l))
        if fixed_brick_clash:
            print("excluded due to fixed-brick clash")
            show_bricks(fixed_brick_clash,"  ")
    print()
    return b


def mins(voxels):
    """minimum x,y,z"""
    return map(min,zip(*voxels))


def maxs(voxels):
    """minimum x,y,z"""
    return map(max,zip(*voxels))


def sizes(voxels):
    """width,height,depth"""
    mnx,mny,mnz = mins(voxels)
    mxx,mxy,mxz = maxs(voxels)
    return 1+mxx-mnx, 1+mxy-mny, 1+mxz-mnz 


def translate(voxels,tx,ty,tz):
    return [ (x+tx, y+ty, z+tz) for x,y,z in voxels ]


def origin(voxels):
    """translate voxels towards 0,0,0"""
    mnx,mny,mnz = mins(voxels)
    return translate(voxels, -mnx, -mny,-mnz)


def origins(bricks):
    return list(map(origin,bricks))


def zero_dup(bricks):
    r = []
    for b in bricks:
        s = set(b)
        if s in r:
            s = set()
        r.append(s)
    return r


def tops(voxels):
    """if the cube containing the voxels
       were a die, with sides numbered 0-6,
       this function returns 6 orientations
       of the die/voxels, each with a different
       side 'uppermost' (x,y,z == x,y,2)

       ..in this order:

            2
         5430
            1
    """
    return [
        voxels,
        rotx(voxels),
        rotx(rotx(rotx(voxels))),
        roty(voxels),
        roty(roty(voxels)),
        roty(roty(roty(voxels))),]


def rots(voxels):
    """if the cube containing the voxels
       were a die, with _some_ face 'uppermost',
       this function returns each of the 4
       orientations which keeps that face
       uppermost
       (with 'tops()', this produces all 24
       possible orientations)
    """
    return [
        voxels,
        rotz(voxels),
        rotz(rotz(voxels)),
        rotz(rotz(rotz(voxels))),]


def orients(voxels):
    """combine tops and rots to get all
       orientations of voxels
    """
    os = []
    for t in tops(voxels):
        os += rots(t)
    return os


def unique(bricks):
    """Find and remove duplicate
       bricks by shifting to origin
        FIXME: this was tricky
    """
    if duplicates_allowed:
        print("Allowing duplicates")
    us = []
    for b in bricks:
        oo = set(origin(b))
        if duplicates_allowed or oo not in us:
            us.append(oo)
    return us
    

def locs(voxels,translateme=True,exclude_centre=False):
    """a list of voxels (at the origin)
       translated to every 'legal' position
       within their cube
    """
    ls = []
    if translateme:
        sx,sy,sz = sizes(voxels)
    else:
        sx,sy,sz = 3,3,3
    for z in range(1 + 3 - sz):
        for y in range(1 + 3 - sy):
            for x in range(1 + 3 - sx):
                translated = translate(voxels,x,y,z)
                if exclude_centre and (1,1,1) in translated:
                    continue
                ls.append(translated)
    return ls

