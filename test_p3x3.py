import unittest


from p3x3 import *


class TestTops(unittest.TestCase):


    def test_tops(self):
        """ tops() should produce:
            -  6 orientations of a set of voxels (brick), such that:
            -  every 'die' face appears uppermost (z==2) once
            We can test this by labelling the centre of each
            'face' of the cube with humbers '1'..'6'.
        """
        b = [(1,1,2,'1'),
             (1,1,0,'6'),
             (1,2,1,'2'),
             (1,0,1,'5'),
             (2,1,1,'3'),
             (0,1,1,'4'),]
        bs = tops(b)
        self.assertEqual(len(bs),6)
        self.assertEqual(bs[0],b)
        centres = ""
        for p in bs:
            self.assertEqual(len(p),len(b))
            for v in p:
                if v[2]==2:
                    centres += v[3]
        self.assertEqual("".join(sorted(centres)),"123456")


class TestRots(unittest.TestCase):


    def test_rots(self):
        """ rots() should prodce:
            - 4 rotations of a brick, such that
            - the same face remains uppermost (z==2)
            - each of the rotations around z appears once
        """
        b = [(1,0,2,'n'),
             (2,1,2,'e'),
             (1,2,2,'s'),
             (0,1,2,'w'),]
        bs = rots(b)
        self.assertEqual(len(bs),4)
        self.assertEqual(bs[0],b) 
        labels = []
        for p in bs:
            d = {str(x)+str(y)+str(z): l for x,y,z,l in p}
            labels.append(d['102'] + d['212'] + d['122'] + d['012'])
        for order in ['nesw','eswn','swne','wnes']:
            self.assertTrue(order in labels)
            

class TestLocs(unittest.TestCase):


    def test_locs(self):
        """ locs() should produce:
            - all valid translations of a block
        """
        b = [(0,0,0)]
        bs = locs(b)
        self.assertEqual(len(bs),3*3*3)
        b = [(0,0,0),(1,1,1)]
        bs = locs(b)
        self.assertEqual(len(bs),2*2*2)
        b = [(0,0,0),(2,2,2)]
        bs = locs(b)
        self.assertEqual(len(bs),1*1*1)
        b = [(0,0,0),(0,0,2)]
        bs = locs(b)
        self.assertEqual(len(bs),3*3*1)



if __name__ == '__main__':
    unittest.main()
