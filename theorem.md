# Theorem: brick0 can't occupy the centre of the cube

1. there are 8 corners but only 7 pieces
2. therefore at least one piece must occupy more than one corner
3. only bricks 0 and 1 are big enough to occupy more than one corner
4. the shape of brick 0 means it can occupy 0 or 2 corners
5. the shape of brick 1 means it can occupy 0 1 or 2 corners
6. this table shows how many corners need to be filled
   by the remaining 5 bricks, according to how many corners
   are occupied by bricks 0 and 1

    brick0\brick1 | 0  |  1 |  2
    --------------|----|----|----
    0             | 8§ | 7§ | 6§
    2             | 6§ | 5  | 4

    note: § indicates too-many corners for 5 bricks to occupy

7. since the only two possible solutions (5.4) are in the 
   second row, brick 0 must, therefore, occupy 2 corners
8. the shape of brick 0 means it cannot occupy two corners
   and the centre at the same time


Note (7) means: ONLY the unrotated/untranslated version of brick0 is needed

# Theorem: brick1 can't occupy the centre of the cube

1. as we saw in the table above, brick 1 must occupy 1 or 2 corners
2. to occupy 2 corners, it must lie along an edge of the cube
3. (as with brick1) if lies along an edge, its shape is such that
   none of its voxels can occupy the centre.
4. it can't occupy a corner if the long part of its shape goes
   through the centre of the cube (the bit that sticks out would
   occupy the centre of an edge)
5. the only way it can occupy 1 corner is to lie on the outside 
   of the cube, betweem two edges - from there the bit that sticks out
   can occupy a corner.

Note (4) means: translations of brick1 which go through the centere of the
cube can be ignored.

